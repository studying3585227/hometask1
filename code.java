import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;


public class Main {
    public static void main(String[] args) {

        List<String> names = List.of("Nicole", "Nika", "milana", "George");
        names.stream()
                .filter(name -> name.length() > 4)
                .filter(Main::upperChecker)
                .forEach(System.out::println);
    }

    public static Boolean upperChecker (String str){
        String regEx = "[A-Z]\\w*";
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }
}
